# Instructions for maintainers

## Building and pushing docker image

You can build and tag docker image in any way you want.
If you want to just build the image and push it use:

```bash
docker login -u <gitlab-email> -p $PAT registry.gitlab.com
docker build -t registry.gitlab.com/gitlab-ci-templates3/gitlab-ci-code-reviewer .
docker push registry.gitlab.com/gitlab-ci-templates3/gitlab-ci-chatgpt
```

PAT is your personal token for Gitlab access.
