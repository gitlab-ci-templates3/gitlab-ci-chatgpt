# gitlab-ci-chatgpt

## AI Code Reviewer

AI Code Reviewer allows you to quickly spot common mistakes in your code.
The diff from the Merge Request is sent to ChatGPT and asked for review.

### How to use it

You can use in locally, or set up as a part of Gitlab CI (preferable).

#### Locally

When using locally, make sure you install the python libraries from the 
requirements.txt file. 
On top of that you will need to have 4 environment variables:
- OPENAI_API_KEY: key to Open AI for API access
- PAT: Personal/Project Access Token to your repository (it will need write access for posting comments in Merge Request)
- CI_PROJECT_PATH: ex. "namespace/project/repository"
- CI_MERGE_REQUEST_IID: internal id for merge request, (not an absolute id, but the id within a repository), ex. 3.

Note that the last two variables are available during Gitlab CI and you only 
need to provide them for local executions.

After having env variables loaded, you can run `python main.py`.

#### Gitlab CI

Add the following two keys to your CI/CD variables:
- OPENAI_API_KEY: key to Open AI for API access
- PAT: Personal/Project Access Token to your repository (it will need write access for posting comments in Merge Request)

Run `python main.py` as part of the CI stage, inside image `registry.gitlab.com/gitlab-ci-templates3/gitlab-ci-chatgpt:latest`.
For more detailed example see `.gitlab-ci.yml` in this repository.
